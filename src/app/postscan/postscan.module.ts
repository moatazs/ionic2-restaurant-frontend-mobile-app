import { NgModule } from '@angular/core';
import { PostscantabsPage } from "./postscantabs/postscantabs";
import { SharedModule } from "../shared/shared.module";
import { ClosePage } from "./pages/close/close";
import { ContactPage } from "./pages/contact/contact";
import { MenuPage } from "./pages/menu/menu";
import { CategoryPage } from "./pages/category/category";
import { ItemPage } from "./pages/item/item";
import { TabsService } from "./shared/tabs.service";
import { MenuService } from './providers/menu-service';
import { CategoryService } from './providers/category-service';
import { ItemService } from './providers/item-service';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        PostscantabsPage,
        ClosePage,
        ContactPage,
        MenuPage,
        CategoryPage,
        ItemPage
        
    ],
    providers: [
        TabsService,
        MenuService,
        CategoryService,
        ItemService
    ],
    entryComponents: [
        PostscantabsPage,
        ClosePage,
        ContactPage,
        MenuPage,
        CategoryPage,
        ItemPage
    ]
})
export class PostscanModule {
}
