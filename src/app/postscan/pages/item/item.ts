import { Component } from '@angular/core';
import { NavController, AlertController, NavParams } from 'ionic-angular';

import { ItemService } from '../../providers/item-service';
// import { CartPage } from "../cart/cart";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-item',
  templateUrl: 'item.html'
})
export class ItemPage {
  // item object
  public item: any;
  public favourite: boolean;
  public itemorder:any;
  constructor(public nav: NavController, public itemService: ItemService,
    public alertController: AlertController, public navParams: NavParams) {
    // get sample data for item
    var item_id = this.navParams.get("id");
    this.loadItem(item_id);
  }

  logForm(form)
  {
    console.log(form);
  }


  loadItem(item_id) {
    this.itemService.load(item_id)
      .then(data => {
        debugger;
        this.item = data;
      });
  }

  // toggle favorite
  toggleFav() {
    this.favourite = !this.favourite;
  }

  // // add item to cart
  addCart() {
    let prompt = this.alertController.create({
      title: 'Quanity',
      message: "",
      inputs: [
        {
          name: 'quantity',
          value: '1'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
            // go to cart
            // this.nav.setRoot(CartPage);
          }
        }
      ]
    });

    prompt.present();
  }
}
