import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, Events } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { AppService } from "./core/app.service";
import { LoginPage } from './auth/pages/login/login';
import { PasscodePage } from './auth/pages/passcode/passcode';
import { TabsPage } from './tabs/tabs/tabs';
import { AuthService } from "./auth/providers/auth.service";
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
import { Headers, Http } from '@angular/http';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    rootPage;
    storage: Storage = new Storage();
    loader: any
    RE_AUTH_URL: string = "http://localhost:8100/api-token-refresh/";
    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    @ViewChild(Nav) nav;

    constructor(platform: Platform, private appService: AppService, private events: Events,
        private auth: AuthService, private loadingCtrl: LoadingController, private http: Http) {
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            StatusBar.styleDefault();
            Splashscreen.hide();


            this.checkAuth();

            this.events.subscribe('loggedIn', authResult => this.proceedWithLogin(authResult));
            this.events.subscribe('loggedOut', () => this.checkAuth());

        });
    }


    proceedWithLogin(authResult) {
        debugger;
        var verified_user = Boolean(authResult.verified);

        if (verified_user) {

            if (authResult.details) {

                this.storage.set('details', authResult.details).then(() => {
                    this.auth.setDetails(authResult.details);
                });
            }

            this.nav.setRoot(TabsPage)
        } else {
            console.log("user needs verification")
            debugger;
            this.nav.setRoot(PasscodePage, { "phonenumber": authResult.phone_number })
        }
    }

    checkAuth() {
        debugger;
        this.auth.authenticatedPromise()
            .then(authenticated => {
                if (authenticated) {
                    this.auth.startupTokenRefresh();
                    this.storage.get('verified').then((verified) => {
                        if (verified) {
                            this.nav.setRoot(TabsPage);
                        } else {
                            this.storage.get('phone_number').then((phone_number) => {
                                this.nav.setRoot(PasscodePage, { "phonenumber": phone_number })
                            });
                        }
                    });

                } else {
                    this.nav.setRoot(LoginPage);
                }
            });


        // this.storage.get('id_token').then((id_token) => {
        //     if (id_token) {
        //         this.nav.setRoot(TabsPage);
        //     } else {
        //         this.nav.setRoot(LoginPage);
        //     }
        // });
    }

    // refreshToken(token)
    // {
    //     var request = {
    //         token: token
    //     }
    //     this.loader = this.loadingCtrl.create({
    //         content: 'Logging in...'
    //     });
    //     this.loader.present();
    //     this.http.post(this.RE_AUTH_URL, JSON.stringify(request), { headers: this.contentHeader })
    //         .map(res => res.json())
    //         .subscribe(
    //         data => {
    //             this.loader.dismiss();
    //             this.nav.setRoot(TabsPage);
    //         },
    //         err => {
    //             this.appService.presentBasicAlert(err,err._body);
    //             this.loader.dismiss();
    //         }
    //         );
    // }

    // if (this.appService.isAuthenticated()) {
    //     this.nav.setRoot(TabsPage);
    // } else {
    //     this.nav.setRoot(LoginPage)
    // }

}
