import { NgModule } from '@angular/core';
import { IonicApp } from 'ionic-angular';
import { MyApp } from './app.component';
import { CoreModule } from "./core/core.module";
import { AuthModule } from "./auth/auth.module";
import { TabsModule } from "./tabs/tabs.module";
import { PostscanModule } from "./postscan/postscan.module";
import { AdminModule } from "./admin/admin.module";
// import { HomePage } from '../pages/home/home';
// import { BrowserModule } from '@angular/platform-browser';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        MyApp,
        // HomePage
    ],
    imports: [
        CoreModule,
        AuthModule,
        TabsModule,
        AdminModule,
        PostscanModule
        // IonicModule.forRoot(MyApp)
        // ,
        // BrowserModule,
        // FormsModule,
        // ReactiveFormsModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        // HomePage
    ],
    providers: [

    ]
})
export class AppModule { }
