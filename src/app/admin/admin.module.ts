import { NgModule } from '@angular/core';
import { OptionsPage } from "./pages/options/options";
import { TablesPage } from "./pages/tables/tables";
import { SeatsPage } from "./pages/seats/seats";
import { SharedModule } from "../shared/shared.module";

@NgModule({
    imports: [
       SharedModule
    ],
    declarations: [
        OptionsPage,
        TablesPage,
        SeatsPage
    ],
    providers: [
        
    ],
    entryComponents: [
        OptionsPage,
        TablesPage,
        SeatsPage
    ]
})
export class AdminModule {
}
