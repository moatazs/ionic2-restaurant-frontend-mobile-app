import { Injectable } from "@angular/core";
import { LoadingController, App } from 'ionic-angular';
import { AppService } from "../../core/app.service"
import { AuthService } from "../../auth/providers/auth.service";
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Headers, Http } from '@angular/http';

@Injectable()
export class ItemService {
    private items: any;
    private loader: any;
    ITEM_URL: string = "http://localhost:8100/orders/nesteditems/";

    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    error: any;

    constructor(private app: App, private loadingCtrl: LoadingController,
        private authHttp: AuthHttp, public appService: AppService) {
        // this.categories = CATEGORIES;
    }

    getAll() {
        return this.items;
    }

    getItem(id) {
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].id === parseInt(id)) {
                return this.items[i];
            }
        }
        return null;
    }

    remove(item) {
        this.items.splice(this.items.indexOf(item), 1);
    }

    load(item_id) {
        debugger;
        // don't have the data yet
        return new Promise(resolve => {
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            this.loader = this.loadingCtrl.create({
                content: 'Getting Item details...'
            });
            this.loader.present();

            this.authHttp.get(this.ITEM_URL + item_id)
                .map(res => res.json())
                .subscribe(
                data => {
                    debugger;
                    this.loader.dismiss();
                    resolve(data);

                },
                err => {
                    debugger;
                    this.error = err;
                    this.loader.dismiss();
                    this.appService.presentBasicAlert(err, err._body);
                }
                );
        });
    }
}
