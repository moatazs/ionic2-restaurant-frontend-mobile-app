import { Component } from '@angular/core';
import { MenuPage } from "../pages/menu/menu";
import { ClosePage } from "../pages/close/close";
import { ContactPage } from "../pages/contact/contact";
import { OptionsPage } from "../../admin/pages/options/options";
import { AuthService } from "../../auth/providers/auth.service";
import { NavParams, LoadingController } from 'ionic-angular';
@Component({
    templateUrl: 'postscantabs.html'
})
export class PostscantabsPage {
    // this tells the tabs component which Pages
    // should be each tab's root Page
    tab1Root: any = MenuPage;
    tab2Root: any = ClosePage;
    tab3Root: any = ContactPage;
    restaurant_id: any;

    constructor(public navParams: NavParams) {
        debugger;
        this.restaurant_id = this.navParams.get("restaurant_id");

    }




}
