import { NgModule } from '@angular/core';
// import { SharedModule } from "../shared/shared.module";
import { LoginPage } from "./pages/login/login";
import { SignupPage } from "./pages/signup/signup";
import { PasscodePage } from "./pages/passcode/passcode";
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { AuthService } from './providers/auth.service';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
// import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "../shared/shared.module";

let storage: Storage = new Storage();

export function getAuthHttp(http) {
    return new AuthHttp(new AuthConfig({
        headerPrefix: 'JWT',
        globalHeaders: [{ 'Accept': 'application/json' }],
        tokenGetter: (() => storage.get('id_token')),
    }), http);
}

@NgModule({
    imports: [
        SharedModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        LoginPage,
        SignupPage,
        PasscodePage
    ],
    providers: [
        AuthService,
        {
            provide: AuthHttp,
            useFactory: getAuthHttp,
            deps: [Http]
        }
    ],
    entryComponents: [
        LoginPage,
        SignupPage,
        PasscodePage
    ]
})
export class AuthModule {
}