import { Component } from '@angular/core';

import { TabsService } from "../../shared/tabs.service";
import { AppService } from "../../../core/app.service";
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { AuthService } from "../../../auth/providers/auth.service";
import { LoginPage } from "../../../auth/pages/login/login";
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Storage } from '@ionic/storage';
import { MenuService } from '../../providers/menu-service';
import { CategoryPage } from '../category/category';

@Component({
    selector: 'page-menu',
    templateUrl: 'menu.html'
})
export class MenuPage {
    local: Storage = new Storage();

    loader: any
    error: string;
    // list of categories and restaurant info
    public data: any;
    public categories: any;


    constructor(public navCtrl: NavController, public navParams: NavParams,
        public menuService: MenuService) {
        debugger;
        var restaurant_id = this.navParams["data"];
        this.loadMenu(restaurant_id);
        // this.categories = menuService.getAll();
    }

    loadMenu(restaurant_id) {
        this.menuService.load(restaurant_id)
            .then(data => {
                debugger;
                this.data = data;
            });
    }

    ionViewWillEnter() {
        console.log("menu will enter");
    }

    ionViewDidEnter() {
        console.log("menu did enter");
    }

    viewCategory(categoryId) {
        this.navCtrl.push(CategoryPage, { id: categoryId });
    }


}
