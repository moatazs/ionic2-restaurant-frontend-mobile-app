import { Component } from '@angular/core';
import { AuthService } from "../../providers/auth.service";
import { Headers, Http } from '@angular/http';
import { JwtHelper, AuthHttp } from 'angular2-jwt';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { NavController } from 'ionic-angular';
import { PasscodePage } from '../passcode/passcode';
import { TabsPage } from '../../../tabs/tabs/tabs';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {
    LOGIN_URL: string = "http://localhost:8100/api-token-auth/";

    // LOGIN_URL: string = "https://restaurant-moatazs.c9users.io/api-token-auth/";

    // When the page loads, we want the Login segment to be selected
    authType: string = "login";
    // We need to set the content type for the server
    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    error: string;
    jwtHelper: JwtHelper = new JwtHelper();
    local: Storage = new Storage();
    user: string;
    loader: any

    constructor(private http: Http, private loadingCtrl: LoadingController, private auth: AuthService,
        public navCtrl: NavController) {

    }

    login(credentials) {
        this.auth.login(credentials);
        // debugger;
        // this.loader = this.loadingCtrl.create({
        //     content: 'Logging in...'
        // });
        // this.loader.present();
        // this.http.post(this.LOGIN_URL, JSON.stringify(credentials), { headers: this.contentHeader })
        //     .map(res => res.json())
        //     .subscribe(
        //     data => {
        //         this.authSuccess(data);
        //         this.loader.dismiss();
        //     },
        //     err => {
        //         this.error = err
        //         this.loader.dismiss();
        //     }
        //     );
    }


    switchToSignupPage() {
        this.navCtrl.push(SignupPage);
    }

    authSuccess(response) {
        debugger;
        var verified_user = Boolean(response.verified);

        if (verified_user) {
            console.log("user is verified")
            this.local.set('id_token', response.token);

            this.local.set('user_type', response.user_type).then(() => {
                this.auth.setUserType(response.user_type);
            });

            this.error = null;

            if (response.details) {

                this.local.set('details', response.details).then(() => {
                    this.auth.setDetails(response.details);
                });
            }

            this.user = this.jwtHelper.decodeToken(response.token).username;
            this.navCtrl.setRoot(TabsPage)
        } else {
            console.log("user needs verification")
            this.navCtrl.setRoot(PasscodePage, { "phonenumber": response.phone_number })
        }


        console.log("Auth Login Success")
    }

    logout() {
        this.loader = this.loadingCtrl.create({
            content: 'logging out...'
        });
        this.loader.present();
        debugger;
        setTimeout(() => {

            debugger;
            this.loader.dismiss();
            this.local.remove('id_token');
            this.user = null;
        }, 1000)
        // this.authHttp.post(this.LOGOUT_URL,'',{})
        //     .map(res => res.json())
        //     .subscribe(
        //     data => {
        //         debugger;
        //         this.loader.dismiss();
        //         // this.navCtrl.pop();
        //         this.local.remove('token');
        //         this.user = null;
        //     },
        //     err => this.error = err
        //     );


    }

}
