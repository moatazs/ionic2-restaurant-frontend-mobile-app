import { Component } from '@angular/core';
import { AuthService } from "../../providers/auth.service";
import { Headers, Http } from '@angular/http';
import { JwtHelper, AuthHttp } from 'angular2-jwt';
import { Storage } from '@ionic/storage';
import { LoadingController, NavController, Events } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { PasscodePage } from '../passcode/passcode';
@Component({
    selector: 'page-sigup',
    templateUrl: 'signup.html'
})
export class SignupPage {
    SIGNUP_URL: string = "http://localhost:8100/register-jwt/";

    // When the page loads, we want the Login segment to be selected
    authType: string = "signup";
    // We need to set the content type for the server
    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    error: string;
    jwtHelper: JwtHelper = new JwtHelper();
    local: Storage = new Storage();
    user: string;
    loader: any
    signupForm: FormGroup;
    submitAttempt: boolean = false;

    constructor(private formBuilder: FormBuilder, private http: Http,
        private loadingCtrl: LoadingController, private auth: AuthService,
        public navCtrl: NavController, public events: Events) {
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/; 
        const regex = /^\+(?:[0-9] ?){6,14}[0-9]$/;
        this.signupForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            passwordConfirm: ['', Validators.required, this.checkPassword],
            email: ['',  Validators.compose([Validators.pattern(emailPattern), Validators.required])],
            dateOfBirth: ['', Validators.required],
            gender: ['', Validators.required],
            phoneNumber: ['', Validators.compose([Validators.pattern(regex), Validators.required])],
        });


    }
    // passwordMatchValidator(formFieldCtrl: FormControl) {
    //     var passwordValue = formFieldCtrl.parent.get('password').value
    //     var passwordConfirmValue = formFieldCtrl.parent.get('passwordConfirm').value
    //     return passwordValue === passwordConfirmValue
    //         ? null : { 'mismatch': true };
    // }

    checkPassword(formFieldCtrl: FormControl): any {

        return new Promise(resolve => {

            //Fake a slow response from server


            var passwordValue = formFieldCtrl.parent.get('password').value
            var passwordConfirmValue = formFieldCtrl.parent.get('passwordConfirm').value
            if (passwordValue === passwordConfirmValue) {

                resolve(null);

            } else {
                resolve({
                    "password mismatch": true
                });
            }


        });
    }

    signupFunc() {
        debugger;
        console.log(this.signupForm.value)
        this.navCtrl.setRoot(PasscodePage,{"phonenumber":this.signupForm.value.phoneNumber})
    }

    signup() {
        var credentials = 
        {
            username: this.signupForm.value.username,
            password: this.signupForm.value.password,
            email: this.signupForm.value.email,
            date_of_birth: this.signupForm.value.dateOfBirth,
            gender: this.signupForm.value.gender,
            phone_number: this.signupForm.value.phoneNumber,
        }

        var login_creds = 
        {
            username: this.signupForm.value.username,
            password: this.signupForm.value.password
        }
        
        debugger;
        this.http.post(this.SIGNUP_URL, JSON.stringify(credentials), { headers: this.contentHeader })
            .map(res => res.json())
            .subscribe(
            data => {
                debugger;
                this.auth.login(login_creds);
            },
            err => this.error = err
            );
    }

    authSuccess(token) {
        debugger;
        // var verified_user = Boolean(response.verified);
        // if(verified_user)
        // {
        //     console.log("user is verified")
        // }else{
        //     console.log("user needs verification")
        // }

        // this.error = null;
        // this.local.set('token', response.token);
        // this.user = this.jwtHelper.decodeToken(response.token).username;
        // this.navCtrl.setRoot(HelloIonicPage);
        console.log("Auth Signup Success")
    }

    logout() {
        this.loader = this.loadingCtrl.create({
            content: 'logging out...'
        });
        this.loader.present();
        debugger;
        setTimeout(() => {

            debugger;
            this.loader.dismiss();
            this.local.remove('id_token');
            this.user = null;
        }, 1000)
        // this.authHttp.post(this.LOGOUT_URL,'',{})
        //     .map(res => res.json())
        //     .subscribe(
        //     data => {
        //         debugger;
        //         this.loader.dismiss();
        //         // this.navCtrl.pop();
        //         this.local.remove('token');
        //         this.user = null;
        //     },
        //     err => this.error = err
        //     );


    }

}
