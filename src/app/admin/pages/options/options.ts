import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { TablesPage } from "../tables/tables";
@Component({
    selector: 'page-options',
    templateUrl: 'options.html'
})
export class OptionsPage {

    serviceName: string = '';
    options: Array<{ name: string }>;
    // items: Array<{ title: string, note: string, icon: string }>;
    constructor(public navCtrl: NavController) {
        this.options = [];
        this.options.push({name: 'Tables'});
        // this.options.push({name: 'Waiters'});
        // this.options.push({name: 'Chefs'});
    }


    logMeOut() {
        // this.appService.logout();
    }

    itemTapped(event, item) {
        debugger;
        if(item.name == "Tables")
        {
            console.log("Go to tables settings");
            this.navCtrl.push(TablesPage);
        }
        // this.navCtrl.push(ItemDetailsPage, {
        //     item: item
        // });
    }

}
