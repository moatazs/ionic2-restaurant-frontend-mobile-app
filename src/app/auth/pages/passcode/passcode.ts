import { Component } from '@angular/core';
import { AuthService } from "../../providers/auth.service";
import { TabsPage } from '../../../tabs/tabs/tabs';
import { Headers, Http } from '@angular/http';
import { JwtHelper, AuthHttp } from 'angular2-jwt';
import { Storage } from '@ionic/storage';
import { NavParams, LoadingController } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup} from '@angular/forms';
import { ToastController } from 'ionic-angular';

@Component({
    selector: 'page-passcode',
    templateUrl: 'passcode.html'
})
export class PasscodePage {
    VERIFY_URL: string = "http://localhost:8100/verifycode/";
    passcodeForm: FormGroup;
    phonenumber: string
    // We need to set the content type for the server
    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    error: string;
    jwtHelper: JwtHelper = new JwtHelper();
    local: Storage = new Storage();
    user: string;
    loader: any

    constructor(private http: Http, private loadingCtrl: LoadingController, private auth: AuthService,
        private formBuilder: FormBuilder, public navCtrl: NavController, private _navParams: NavParams,
        public toastCtrl: ToastController) {
        this.phonenumber = this._navParams.get("phonenumber");

        const regex = /^\d+$/;
        this.passcodeForm = this.formBuilder.group({
            passcode: ['', Validators.compose([Validators.pattern(regex), Validators.required])],
        });
    }

    verify() {
        debugger;
        var data = this.passcodeForm.value;
        console.log(this.passcodeForm.value);
        console.log(data);
        var credentials=
        {
            phone_number: this.phonenumber,
            device_id: "",
            passcode: data.passcode
        }
        this.loader = this.loadingCtrl.create({
            content: 'Verifying...'
        });
        this.loader.present();
        // let loader = this.loadingController.create({
        //     content: "Loggin in ..."
        // });
        // loader.present();
        this.http.post(this.VERIFY_URL, JSON.stringify(credentials), { headers: this.contentHeader })
            .map(res => res.json())
            .subscribe(
            data => {
                this.loader.dismiss();
                this.local.set('verified', true).then(() => {
                    this.navCtrl.setRoot(TabsPage)
                });
            },
            err => 
            {
                this.error = err
                this.loader.dismiss();
            }
            );
    }

    resendPasscode() {
        console.log("calling API to resend passcode");
        let toast = this.toastCtrl.create({
            message: 'Passcode has been resent to ' + this.phonenumber,
            duration: 3000
        });
        toast.present();
    }


    

}
