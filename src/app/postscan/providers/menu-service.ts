import { Injectable } from "@angular/core";
import { LoadingController, App } from 'ionic-angular';
import { AppService } from "../../core/app.service"
import { AuthService } from "../../auth/providers/auth.service";
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Headers, Http } from '@angular/http';

@Injectable()
export class MenuService {
    private menu: any;
    private loader: any;
    RESTAURANT_URL: string = "http://localhost:8100/restaurant/nestedrestaurant/";

    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    error: any;

    constructor(private app: App, private loadingCtrl: LoadingController, 
      private auth: AuthService, private authHttp: AuthHttp,
        public appService: AppService) {

    }

    getAll() {
        return this.menu;
    }

    getItem(id) {
        for (var i = 0; i < this.menu.length; i++) {
            if (this.menu[i].id === parseInt(id)) {
                return this.menu[i];
            }
        }
        return null;
    }

    remove(item) {
        this.menu.splice(this.menu.indexOf(item), 1);
    }


    load(restaurant_id) {
      debugger;
        // don't have the data yet
        return new Promise(resolve => {
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            this.loader = this.loadingCtrl.create({
                content: 'Getting Restaurant Menu...'
            });
            this.loader.present();

            this.authHttp.get(this.RESTAURANT_URL + restaurant_id)
                .map(res => res.json())
                .subscribe(
                data => {
                    debugger;
                    this.loader.dismiss();
                    
                    this.menu = data;
                    resolve(this.menu);

                },
                err => {
                    debugger;
                    this.error = err;
                    this.loader.dismiss();
                    this.appService.presentBasicAlert(err, err._body);
                }
                );
        });
    }
}
