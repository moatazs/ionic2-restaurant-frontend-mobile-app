import { NgModule, ErrorHandler } from '@angular/core';
import { IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from '../app.component';
import { AppService } from "./app.service";

@NgModule({
    imports: [
        IonicModule.forRoot(MyApp),
    ],
    declarations: [
    ],
    providers: [
        AppService,
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }
    ],
    exports: [
        IonicModule
    ]
})
export class CoreModule {
}
