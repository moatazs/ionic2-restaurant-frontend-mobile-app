import { NgModule } from '@angular/core';
import { TabsPage } from "./tabs/tabs";
import { SharedModule } from "../shared/shared.module";
import { ScannerPage } from "./pages/scanner/scanner";
import { ContactPage } from "./pages/contact/contact";
import { HomePage } from "./pages/home/home";
import { TabsService } from "./shared/tabs.service";


@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        TabsPage,
        ScannerPage,
        ContactPage,
        HomePage
        
    ],
    providers: [
        TabsService
    ],
    entryComponents: [
        TabsPage,
        ScannerPage,
        ContactPage,
        HomePage
    ]
})
export class TabsModule {
}
