import { Component } from '@angular/core';

import { NavController, NavParams, LoadingController, App } from 'ionic-angular';
import { BarcodeScanner } from 'ionic-native';
import { AppService } from "../../../core/app.service"
import { AuthService } from "../../../auth/providers/auth.service";
import { TabsPage } from "../../../tabs/tabs/tabs";
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Headers, Http } from '@angular/http';

@Component({
    selector: 'page-close',
    templateUrl: 'close.html'
})
export class ClosePage {

    loader: any
    // needed to get restaurant info and what no
    RESTAURANT_URL: string = "http://localhost:8100/restaurant/nestedrestaurant/";

    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    error: any;

    constructor(private app: App, private loadingCtrl: LoadingController, private auth: AuthService,
        public navCtrl: NavController, public navParams: NavParams, private authHttp: AuthHttp,
        public appService: AppService) {
        debugger;
        const root = this.app.getRootNav();
        root.popToRoot(TabsPage);
        console.log("close constructor");
    }

    ionViewWillEnter() {
        console.log("close will enter");
    }

    ionViewDidEnter() {
        console.log("close did enter");
    }

}
