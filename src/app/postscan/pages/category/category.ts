import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ItemPage } from '../item/item';
import { CategoryService } from '../../providers/category-service';


@Component({
  selector: 'page-category',
  templateUrl: 'category.html'
})
export class CategoryPage {
  // category object
  public category: any;

  constructor(public nav: NavController, public categoryService: CategoryService,
    public navParams: NavParams) {
    // get first category as sample data
    var category_id = this.navParams.get("id");
    this.loadCategory(category_id);
    // this.category = categoryService.getItem(1);
  }

  loadCategory(category_id) {
    this.categoryService.load(category_id)
      .then(data => {
        debugger;
        this.category = data;
        // this.categories = data.categories;
      });
  }

  // view item detail
  viewItem(id) {
    this.nav.push(ItemPage, { id: id })
  }
}
