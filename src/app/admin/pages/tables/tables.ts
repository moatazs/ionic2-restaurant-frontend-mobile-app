import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { AuthService } from "../../../auth/providers/auth.service";
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { AlertController } from 'ionic-angular';
import { AppService } from "../../../core/app.service"
import { Headers, Http} from '@angular/http';
import { SeatsPage } from "../seats/seats"
/*
  Generated class for the Tables page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-tables',
    templateUrl: 'tables.html'
})
export class TablesPage {

    items: Array<{ number: number, id: number }> = [];
    loader: any
    TABLES_URL: string = "http://localhost:8100/restaurant/tables/";
    restaurant_id: number
    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    error: string;

    constructor(private loadingCtrl: LoadingController, private auth: AuthService,
        public navCtrl: NavController, public navParams: NavParams, private authHttp: AuthHttp,
        public alertCtrl: AlertController, public appService: AppService) {
        debugger;
        this.loader = this.loadingCtrl.create({
            content: 'Getting tables...'
        });
        this.loader.present();

        this.authHttp.get(this.TABLES_URL)
            .map(res => res.json())
            .subscribe(
            data => {
                debugger;
                this.loader.dismiss();

                for (var i = 0; i < data.length; i++) {
                    this.items.push({ number: data[i].number, id: data[i].id });
                }

            },
            err => {
                this.loader.dismiss();
                this.error = err
                console.log(err);
                this.appService.presentBasicAlert(err,err._body);
            }
            );

    }

    removeItem(item) {
        let confirm = this.alertCtrl.create({
            title: 'Delete this table?',
            message: 'Are you sure you want to delete this table? This remove the table with all its seats.',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: () => {
                        this.doRemoveItem(item);
                    }
                }
            ]
        });
        confirm.present();
    }

    doRemoveItem(item) {
        this.loader = this.loadingCtrl.create({
            content: 'Deleting table...'
        });
        this.loader.present();
        this.authHttp.delete(this.TABLES_URL + item.id)
            .map(res => res.json())
            .subscribe(
            data => {
                for (var i = 0; i < this.items.length; i++) {

                    if (this.items[i] == item) {
                        this.items.splice(i, 1);
                    }
                }
                this.loader.dismiss();
            },
            err => {
                this.error = err
                this.appService.presentToast(err);
                this.loader.dismiss()
            }
            );
    }

    addItem() {
        let prompt = this.alertCtrl.create({
            title: 'Add new table',
            message: "Enter new table number",
            inputs: [
                {
                    name: 'number',
                    placeholder: 'Number',
                    type: 'number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Add',
                    handler: data => {
                        var valid = true;
                        for (var i = 0; i < this.items.length; i++) {
                            if (this.items[i].number == data.number) {
                                this.appService.presentBasicAlert("Invalid Number", "Table number already exists");
                                valid = false;
                                break;
                            }
                        }
                        if (valid) {
                            this.doAddItem(data.number);
                        }

                    }
                }
            ]
        });
        prompt.present();
    }

    doAddItem(tableNumber) {
        var new_table = {
            number: tableNumber
        }
        debugger;
        this.loader = this.loadingCtrl.create({
            content: 'Adding table'
        });
        this.loader.present();


        this.authHttp.post(this.TABLES_URL, JSON.stringify(new_table), { headers: this.contentHeader })
            .map(res => res.json())
            .subscribe(
            data => {
                debugger;
                var added = false;

                for (var i = 0; i < this.items.length; i++) {
                    if (this.items[i].number > data.number) {
                        this.items.splice(i, 0, { number: data.number, id: data.id });
                        added = true;
                        break;
                    }
                }
                if (!added) {
                    this.items.push({ number: data.number, id: data.id });
                }


                this.appService.presentBasicAlert("Table has been created!", "");
                this.loader.dismiss();
            },
            err => {
                this.error = err
                this.appService.presentBasicAlert("Error", this.error);
                this.loader.dismiss();
            }
            );
    }

    getSeats(item)
    {
        this.navCtrl.push(SeatsPage, {"table": item.id})
    }

    editItem(item)
    {
        let prompt = this.alertCtrl.create({
            title: 'Edit Table number',
            message: "Enter new table number",
            inputs: [
                {
                    name: 'number',
                    placeholder: 'Number',
                    type: 'number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Add',
                    handler: data => {
                        var valid = true;
                        for (var i = 0; i < this.items.length; i++) {
                            if (this.items[i].number == data.number) {
                                this.appService.presentBasicAlert("Invalid Number", "Table number already exists");
                                valid = false;
                                break;
                            }
                        }
                        if (valid) {
                            this.doEditItem(item,data.number);
                        }

                    }
                }
            ]
        });
        prompt.present();
    }

    doEditItem(item,newTableNumber)
    {
        var new_table = {
            number: newTableNumber
        }
        debugger;
        this.loader = this.loadingCtrl.create({
            content: 'Renaming table...'
        });
        this.loader.present();


        this.authHttp.patch(this.TABLES_URL + item.id + "/", JSON.stringify(new_table), { headers: this.contentHeader })
            .map(res => res.json())
            .subscribe(
            data => {
                for (var i = 0; i < this.items.length; i++) {

                    if (this.items[i] == item) {
                        this.items.splice(i, 1);
                    }
                }
                var added = false;

                for (var i = 0; i < this.items.length; i++) {
                    if (this.items[i].number > data.number) {
                        this.items.splice(i, 0, { number: data.number, id: data.id });
                        added = true;
                        break;
                    }
                }
                if (!added) {
                    this.items.push({ number: data.number, id: data.id });
                }


                this.appService.presentBasicAlert("Table renamed!", "");
                this.loader.dismiss();
            },
            err => {
                this.error = err
                this.appService.presentBasicAlert("Error", this.error);
                this.loader.dismiss();
            }
            );
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad TablesPage');
    }

}
