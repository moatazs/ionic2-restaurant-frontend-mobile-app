import { Component } from '@angular/core';
import { HomePage } from "../pages/home/home";
import { ScannerPage } from "../pages/scanner/scanner";
import { ContactPage } from "../pages/contact/contact";
import { OptionsPage } from "../../admin/pages/options/options";
import { AuthService } from "../../auth/providers/auth.service";
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage {
    // this tells the tabs component which Pages
    // should be each tab's root Page
    tab1Root: any = HomePage;
    tab2Root: any = ScannerPage;
    tab3Root: any = ContactPage;
    tab4Root: any = OptionsPage;
    userType: string;
    owner: boolean
    storage: Storage = new Storage();

    constructor(private auth: AuthService, public navCtrl: NavController) {

            if (this.auth.getUserType() == "OWNER") {
                this.owner = true;
            }

        
    }




}
