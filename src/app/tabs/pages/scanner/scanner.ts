import { Component } from '@angular/core';

import { NavController, NavParams, LoadingController, App } from 'ionic-angular';
import { BarcodeScanner } from 'ionic-native';
import { AppService } from "../../../core/app.service"
import { AuthService } from "../../../auth/providers/auth.service";
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { HomePage } from '../home/home'
import { Headers, Http } from '@angular/http';
import { PostscantabsPage } from "../../../postscan/postscantabs/postscantabs"
@Component({
    selector: 'page-scanner',
    templateUrl: 'scanner.html'
})
export class ScannerPage {

    loader: any
    // needed to get restaurant info and what no
    RESTAURANT_URL: string = "http://localhost:8100/restaurant/nestedrestaurant/";

    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    error: any;

    constructor(private app: App, private loadingCtrl: LoadingController, private auth: AuthService,
        public navCtrl: NavController, public navParams: NavParams, private authHttp: AuthHttp,
        public appService: AppService) {
        debugger;

        console.log("constructor");
    }

    ionViewWillEnter() {
        
    }

    ionViewDidEnter() {
        var response = '{"restaurant":1}';
        // BarcodeScanner.scan().then((barcodeData) => {
        //     if (!barcodeData.cancelled) {
        //         this.appService.presentToast(barcodeData.text);
        //     } else {
        //         this.appService.presentToast("Cancelled");
        //     }

        // }, (err) => {
        //     this.appService.presentBasicAlert("error", err);
        // });

        var lJsonString = JSON.parse(response);
        if (lJsonString.restaurant) {

            this.navCtrl.parent.select(0);
            const root = this.app.getRootNav();
            root.push(PostscantabsPage, { "restaurant_id": lJsonString.restaurant });
        } else {
            this.appService.presentBasicAlert("Invalid QR scan", "");
        }
    }


    goToRestaurantMenu(scannedText) {
        var lJsonString = JSON.parse(scannedText);
        if (lJsonString.restaurant) {

            this.loader = this.loadingCtrl.create({
                content: 'Getting Restaurant Menu...'
            });
            this.loader.present();

            this.authHttp.get(this.RESTAURANT_URL + lJsonString.restaurant)
                .map(res => res.json())
                .subscribe(
                data => {
                    debugger;
                    this.loader.dismiss();
                    // var parent = this.navCtrl.parent;
                    // parent.setRoot(PostscantabsPage,{"data":data});
                    this.navCtrl.parent.select(0);
                    const root = this.app.getRootNav();
                    root.push(PostscantabsPage, { "data": data });
                },
                err => {
                    debugger;
                    this.error = err;
                    this.loader.dismiss();
                    this.appService.presentBasicAlert(err, err._body);
                }
                );

        } else {
            this.appService.presentToast("Can not find restaurant id");
        }
    }

}
