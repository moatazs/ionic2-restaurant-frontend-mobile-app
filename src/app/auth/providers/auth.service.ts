import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { AuthHttp, JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { LoadingController, Events } from 'ionic-angular';
import { Headers, Http } from '@angular/http';
import { AppService } from "../../core/app.service"

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {

    jwtHelper: JwtHelper = new JwtHelper();
    storage: Storage = new Storage();
    refreshSubscription: any;
    user: Object;
    idToken: string;
    userType: string;
    restaurant_id: number
    loader: any
    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    error: string;

    RE_AUTH_URL: string = "http://localhost:8100/api-token-refresh/";
    LOGIN_URL: string = "http://localhost:8100/api-token-auth/";

    constructor(private authHttp: AuthHttp, private loadingCtrl: LoadingController,
        private http: Http, public events: Events, public appService: AppService) {

        this.storage.get('id_token').then(token => {
            this.idToken = token;
        });
        this.storage.get('user_type').then(user_type => {
            this.userType = user_type;
        });

        this.events.subscribe('authenticated', authResult => {
            debugger;
            this.storage.set('id_token', authResult.token);
            this.idToken = authResult.token;

            this.storage.set('refresh_token', authResult.token);
            // Schedule a token refresh
            this.scheduleRefresh();

            this.storage.set('user_type', authResult.user_type);
            this.setUserType(authResult.user_type);
            debugger;
            this.storage.set('phone_number', authResult.phone_number);
            this.storage.set('verified', authResult.verified);

            this.events.publish('loggedIn', authResult);
            
        });

    }

    public login(credentials) {
        debugger;
        this.loader = this.loadingCtrl.create({
            content: 'Logging in...'
        });
        this.loader.present();
        this.http.post(this.LOGIN_URL, JSON.stringify(credentials), { headers: this.contentHeader })
            .map(res => res.json())
            .subscribe(
            data => {
                debugger;
                this.loader.dismiss();
                this.events.publish('authenticated', data);

            },
            err => {
                this.loader.dismiss();
                this.appService.presentBasicAlert(err._body,"");
                this.error = err

            }
            );
    }


    
    public logout() {
        this.storage.clear();
        this.idToken = null;
        
        // Unschedule the token refresh
        this.unscheduleRefresh();
    }

    public unscheduleRefresh() {
        // Unsubscribe from the refresh
        if (this.refreshSubscription) {
            this.refreshSubscription.unsubscribe();
        }
    }


    public scheduleRefresh() {
        // If the user is authenticated, use the token stream
        // provided by angular2-jwt and flatMap the token

        let source = Observable.of(this.idToken).flatMap(
            token => {
                debugger;
                // The delay to generate in this case is the difference
                // between the expiry time and the issued at time
                var asdf = this.jwtHelper.decodeToken(token);
                let jwtIat = this.jwtHelper.decodeToken(token).orig_iat;
                let jwtExp = this.jwtHelper.decodeToken(token).exp;
                let iat = new Date(0);
                let exp = new Date(0);

                let delay = (exp.setUTCSeconds(jwtExp) - iat.setUTCSeconds(jwtIat));

                return Observable.interval(delay);
            });

        this.refreshSubscription = source.subscribe(() => {
            this.getNewJwt();
        });
    }


    public startupTokenRefresh() {
        // If the user is authenticated, use the token stream
        // provided by angular2-jwt and flatMap the token
        debugger;
        let source = Observable.of(this.idToken).flatMap(
            token => {
                // Get the expiry time to generate
                // a delay in milliseconds
                let now: number = new Date().valueOf();
                let jwtExp: number = this.jwtHelper.decodeToken(token).exp;
                let exp: Date = new Date(0);
                exp.setUTCSeconds(jwtExp);
                let delay: number = exp.valueOf() - now;

                if (delay < 0) {
                    return Observable.never();
                }

                // Use the delay in a timer to
                // run the refresh at the proper time
                return Observable.timer(delay);
            });

        // Once the delay time from above is
        // reached, get a new JWT and schedule
        // additional refreshes
        source.subscribe(() => {
            this.getNewJwt();
            this.scheduleRefresh();
        });

    }

    public getNewJwt() {
        // Get a new JWT from Auth0 using the refresh token saved
        // in local storage
        debugger;
        this.storage.get('refresh_token').then(token => {
            var request = {
                token: token
            }
            debugger;
            this.http.post(this.RE_AUTH_URL, JSON.stringify(request), { headers: this.contentHeader })
                .map(res => res.json())
                .subscribe(
                data => {
                    debugger;
                    this.storage.set('id_token', data.token);
                    this.idToken = data.token;

                },
                err => {
                    alert(err);
                }
                );

        }).catch(error => {
            console.log(error);
        });

    }

    public authenticated() {
        this.storage.get('id_token').then(token => {
            return tokenNotExpired('id_token', token);
        });
    }

    public authenticatedPromise() {
        if (this.idToken) {
            return Promise.resolve(true);
        }

        return new Promise(resolve => {
            this.storage.get('id_token').then(token => {
                if (token) {
                    this.idToken = token;
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        });
    }

    public setUserType(userType) {
        this.userType = userType;
    }

    public getUserType() {
        return this.userType;
    }

    public isOwner() {
        debugger;
        return this.getUserType() == "OWNER";
    }

    public setDetails(details) {
        if (details.restaurant) {
            this.restaurant_id = details.restaurant
        }
    }

    public getRestaurantId() {
        return this.restaurant_id;
    }


}
