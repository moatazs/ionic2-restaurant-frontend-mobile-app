import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { AuthService } from "../../../auth/providers/auth.service";
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { AlertController } from 'ionic-angular';
import { AppService } from "../../../core/app.service"
import { Headers, Http } from '@angular/http';
import { BarcodeScanner } from 'ionic-native';
/*
  Generated class for the Seats page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-seats',
    templateUrl: 'seats.html'
})
export class SeatsPage {

    table: number;
    items: Array<{ number: number, id: number }> = [];
    loader: any
    SEATS_URL: string = "http://localhost:8100/restaurant/seats/";
    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    error: any;

    constructor(private loadingCtrl: LoadingController, private auth: AuthService,
        public navCtrl: NavController, public navParams: NavParams, private authHttp: AuthHttp,
        public alertCtrl: AlertController, public appService: AppService) {
        this.table = this.navParams.get("table");
        debugger;
        this.loader = this.loadingCtrl.create({
            content: 'Getting seats...'
        });
        this.loader.present();

        this.authHttp.get(this.SEATS_URL + "?tablepk=" + this.table)
            .map(res => res.json())
            .subscribe(
            data => {
                debugger;
                this.loader.dismiss();

                for (var i = 0; i < data.length; i++) {
                    this.items.push({ number: data[i].number, id: data[i].id });
                }

            },
            err => {
                this.error = err;
                this.loader.dismiss();
                this.appService.presentBasicAlert(err,err._body);
            }
            );

    }

    removeItem(item) {
        this.loader = this.loadingCtrl.create({
            content: 'Deleting seat...'
        });
        this.loader.present();
        this.authHttp.delete(this.SEATS_URL + item.id)
            .map(res => res.json())
            .subscribe(
            data => {
                for (var i = 0; i < this.items.length; i++) {

                    if (this.items[i] == item) {
                        this.items.splice(i, 1);
                    }
                }
                this.loader.dismiss();
            },
            err => {
                this.error = err
                this.appService.presentToast(err);
                this.loader.dismiss()
            }
            );
    }

    addItem() {
      // this.promptItem("restaurant#12-asersf");
      // debugger;
        BarcodeScanner.scan().then((barcodeData) => {
            if (!barcodeData.cancelled) {
                this.promptItem(barcodeData.text);
            } else {
                this.appService.presentToast("Cancelled");
            }

        }, (err) => {
            this.appService.presentBasicAlert("error", err);
        });
    }


    promptItem(barcodeText) {
        let prompt = this.alertCtrl.create({
            title: 'Add new seat',
            message: "Enter new seat number",
            inputs: [
                {
                    name: 'number',
                    placeholder: 'Number',
                    type: 'number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Add',
                    handler: data => {
                        var valid = true;
                        for (var i = 0; i < this.items.length; i++) {
                            if (this.items[i].number == data.number) {
                                this.appService.presentBasicAlert("Invalid Number", "Seat number already exists");
                                valid = false;
                                break;
                            }
                        }
                        if (valid) {
                            this.doAddItem(data.number,barcodeText);
                        }
                    }
                }
            ]
        });
        prompt.present();
    }

    doAddItem(seatNumber,barcodeText) {
        var new_seat = {
            number: seatNumber,
            qr_string: barcodeText,
            table: this.table
        }
        debugger;
        this.loader = this.loadingCtrl.create({
            content: 'Adding seat'
        });
        this.loader.present();


        this.authHttp.post(this.SEATS_URL, JSON.stringify(new_seat), { headers: this.contentHeader })
            .map(res => res.json())
            .subscribe(
            data => {
                debugger;
                var added = false;

                for (var i = 0; i < this.items.length; i++) {
                    if (this.items[i].number > data.number) {
                        this.items.splice(i, 0, { number: data.number, id: data.id });
                        added = true;
                        break;
                    }
                }
                if (!added) {
                    this.items.push({ number: data.number, id: data.id });
                }

                this.appService.presentBasicAlert("Seat has been added!", "");
                this.loader.dismiss();
            },
            err => {
                this.error = err
                this.appService.presentBasicAlert("Error", this.error._body);
                this.loader.dismiss();
            }
            );
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SeatsPage');
    }

}
