import { Injectable } from '@angular/core';
import { Events } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { ToastController, AlertController } from 'ionic-angular';

@Injectable()
export class AppService {
    storage: Storage = new Storage();
    idToken: any;
    
    constructor(private events: Events, public toastCtrl: ToastController, private alertCtrl: AlertController) {

    }

    isAuthenticated() {

    }

    getServerUrl()
    {
        return "http://localhost:8100/";
    }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000
        });
        toast.present();
    }

    presentBasicAlert(title, msg) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: msg,
            buttons: ['Dismiss']
        });
        alert.present();
    }

    // login() {
    //   this.setToken();

    //   this.events.publish('loggedIn');
    // }

    // logout() {
    //   this.deleteToken();

    //   this.events.publish('loggedOut');
    // }

    // private getToken() {
    //   return window.localStorage.getItem('myAppToken');
    // }

    // private setToken() {
    //   return window.localStorage.setItem('myAppToken', 'fakeToken');
    // }

    // private deleteToken() {
    //   return window.localStorage.removeItem('myAppToken');
    // }
}
