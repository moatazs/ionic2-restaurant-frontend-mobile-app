import { Component } from '@angular/core';

import { TabsService } from "../../shared/tabs.service";
import { AppService } from "../../../core/app.service";
import { NavController, NavParams, LoadingController, App } from 'ionic-angular';
import { AuthService } from "../../../auth/providers/auth.service";
import { LoginPage } from "../../../auth/pages/login/login";
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    local: Storage = new Storage();
    serviceName: string = 'tabs service';
    loader: any
    error: string;
    TABLES_URL: string = "http://localhost:8100/restaurant/tables/";
    LOGOUT_URL: string = "http://localhost:8100/auth/logout/";
    constructor(private loadingCtrl: LoadingController, private auth: AuthService,
        public navCtrl: NavController, public navParams: NavParams, 
        private authHttp: AuthHttp, public appService: AppService, public app: App) {

    }


    logMeOut() {

        this.loader = this.loadingCtrl.create({
            content: 'Logging out...'
        });
        this.loader.present();
        debugger;

        this.authHttp.post(this.LOGOUT_URL, '', {})
            .map(res => res.json())
            .subscribe(
            data => {
                this.local.clear();
                this.loader.dismiss();
                const root = this.app.getRootNav();
                root.setRoot(LoginPage);
                // this.user = null;
            },
            err => {
                this.local.clear();
                this.loader.dismiss();
                const root = this.app.getRootNav();
                root.setRoot(LoginPage);
                this.appService.presentBasicAlert(err, "");
            }
            );
    }

}
